import glob
import os
import pickle

import hashlib

hash_list_file = r"res/directoryHash.pkl"


def hash_folder(dir_path: str):
    """ Creates a unique hash for a folder, which would change if filenames change
    The hash can change if a new file is created, if an old file is removed, or a filename changed. In such situations,
    it means the subtitles extraction must be done again

    :param dir_path: Directory to hash
    :return: hash number
    """
    file_list = glob.glob(os.path.join(dir_path, "**/*"), recursive=True)
    file_list.sort()

    file_string = ",".join(file_list)
    folder_hash = hashlib.md5(file_string.encode("utf-8")).hexdigest()

    return folder_hash


def has_directory_changed(dir_path: str):
    """ Return a boolean : True if the folder files has changed. False otherwise

    :param dir_path:
    :return:
    """
    # Load previous dirHash for folder if exists
    try:
        prev_hash = load_hash_dict()[dir_path]
    except KeyError:
        return True

    # Compare old to new
    new_hash = hash_folder(dir_path)
    return new_hash != prev_hash


def load_hash_dict():
    """ Loads the directory of the already-hashed folders from the pickle savefile

    :return:
    """
    try:
        with open(hash_list_file, 'rb') as f:
            return pickle.load(f)
    except FileNotFoundError:
        return dict()


def update_folder_hash(dir_path: str):
    """ Updates the hash for one folder in the pickle savefile

    :param dir_path:
    :return:
    """
    hash_dict = load_hash_dict()

    hash_dict[dir_path] = hash_folder(dir_path)

    with open(hash_list_file, 'wb') as f:
        pickle.dump(hash_dict, f, pickle.HIGHEST_PROTOCOL)
