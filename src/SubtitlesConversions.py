import logging
import os
import subprocess

from .ConfigUtils import Config


def pgs_track_to_srt_file(movie_path: str, subtitle_track: dict, srt_filepath: str):
    """

    :param movie_path:
    :param subtitle_track:
    :param srt_filepath:
    :return: srt_filepath
    """

    track_num = subtitle_track['properties']["number"]
    lang = subtitle_track['properties']['language']
    tesseract_lang = "fra" if lang == "fre" else lang

    logging.debug(f"Converting PGS track #{track_num} to srt")

    # Converting
    verbose = (logging.root.level <= logging.DEBUG)
    subprocess.run(
        ["dotnet", Config.PGS_TO_SRT_DLL,
         "--input", movie_path,
         "--output", srt_filepath,
         "--track", str(track_num),
         "--tesseractlanguage", tesseract_lang],
        stdout=None if verbose else subprocess.DEVNULL,
        stderr=None if verbose else subprocess.STDOUT
    )

    logging.debug(f"Converted")

    return srt_filepath


def pgs_file_to_srt(pgs_filepath: str, subtitle_track: dict, srt_filepath: str):
    """

    :param pgs_filepath:
    :param subtitle_track:
    :param srt_filepath:
    :return: srt_filepath
    """

    lang = subtitle_track['properties']['language']
    tesseract_lang = "fra" if lang == "fre" else lang

    logging.debug(f"Converting PGS file {os.path.split(pgs_filepath)[1]} to srt")

    # Converting
    verbose = (logging.root.level <= logging.DEBUG)
    subprocess.run(
        ["dotnet", Config.PGS_TO_SRT_DLL,
         "--input", pgs_filepath,
         "--output", srt_filepath,
         "--tesseractlanguage", tesseract_lang],
        stdout=None if verbose else subprocess.DEVNULL,
        stderr=None if verbose else subprocess.STDOUT
    )

    logging.debug(f"Converted")

    return srt_filepath


def convert_sub_to_pgs(source_sub_path: str):
    """
    java -jar BDSup2Sub.jar .\test.sub -o test.sup

    :return: pgs_filepath
    """

    logging.debug(f"Converting Image sub file (not PGS) {os.path.split(source_sub_path)[1]} to pgs")

    # Replacing extension with ".sup"
    pgs_filepath = os.path.splitext(source_sub_path)[0] + ".sup"

    # We use BDSup2Sub.jar to convert any image to PGS as a .sup file
    verbose = (logging.root.level <= logging.DEBUG)
    subprocess.run(
        ["java", "-jar",
         Config.BD_SUP2SUB_JAR, source_sub_path,
         "-o", pgs_filepath],
        stdout=None if verbose else subprocess.DEVNULL,
        stderr=None if verbose else subprocess.STDOUT
    )

    logging.debug(f"Converted")

    return pgs_filepath
