import glob
import os

import yaml


def _find_config_file() -> str:
    """ Find the path of the config file "config.yaml" in project dir """
    cwd = os.getcwd()
    return glob.glob(os.path.join(cwd, "**/config.yaml"), recursive=True)[0]


def get_config_dict() -> dict:
    """ Loads the config file and returns it as a disctionary """
    config_filepath = _find_config_file()

    with open(config_filepath, "r") as f:
        return yaml.safe_load(f)


class Config:
    _config_filepath: str = _find_config_file()
    _config: dict = get_config_dict()

    MKV_MERGE: str = _config["MKVTOOLNIX"]["mkvmerge_path"]
    MKV_EXTRACT: str = _config["MKVTOOLNIX"]["mkvextract_path"]
    PGS_TO_SRT_DLL: str = _config["PgsToSrt_dllpath"]
    BD_SUP2SUB_JAR: str = _config["BDSup2Sub_jarpath"]
