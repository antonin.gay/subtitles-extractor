""" This file contains functions for operations on mkv files

They mostly are subprocess calls to mkvmerge and mkvextract
"""

import json
import logging
import os
import subprocess
from typing import List

from .ConfigUtils import Config

# *** Parameters


pgs_codec = "S_HDMV/PGS".lower()
image_subtitles_codecs = [pgs_codec, "S_VOBSUB", "VobSub", "HDMV"]
text_subtitles_codecs = ["SubRip/SRT", "s_text/utf8", "s_text/ass"]

image_subtitles_codecs = list(map(str.lower, image_subtitles_codecs))
text_subtitles_codecs = list(map(str.lower, text_subtitles_codecs))


# *** End parameters


def get_sub_tracks(mkv_filepath: str) -> List[dict]:
    """ List all the subtitles tracks inside a mkv file

    :param mkv_filepath: path to the mkv file
    :return: subtitles_tracks - List of subtitles track as dictionaries
    """
    logging.debug(f"Getting List of subtitles tracks from {os.path.split(mkv_filepath)[1]}")

    # We extract info from the file, using mkvmerge and reading its output as JSON
    a = subprocess.run([Config.MKV_MERGE, "-J", mkv_filepath],
                       capture_output=True, encoding="utf-8")
    file_info = json.loads(a.stdout)

    # We extract the subtitles tracks from all the tracks
    tracks = file_info["tracks"]
    subtitles_tracks = [track for track in tracks if track["type"] == "subtitles"]

    return subtitles_tracks


def sort_sutitles(subtitles: List[dict]) -> [List[dict], List[dict]]:
    """ Sort subtitles in two lists : text_subtitles and image_subtitles

    :param subtitles: List of subtitles track as dictionaries
    :return: image_subtitles, text_subtitles - list of subtitles tracks
    """

    # We analyze each subtitles to classify it depending if image or text codec
    logging.debug("Sorting subtitles between Text and Image")
    image_subtitles, text_subtitles = [], []

    for sub in subtitles:
        # codec: str = sub["codec"]
        codec: str = sub["properties"]["codec_id"].lower()

        if codec in image_subtitles_codecs:
            image_subtitles.append(sub)

        elif codec in text_subtitles_codecs:
            text_subtitles.append(sub)

        else:
            logging.warning(f"**********\nUnknown subtitles codec {codec}. "
                            f"\nDefine it either as an image or a text subtitle. "
                            f"\nBy default, it will not be processed\n**********")
            # image_subtitles.append(sub)

    logging.debug(f"Found {len(text_subtitles)} text subtitles and {len(image_subtitles)} image subtitles")

    return image_subtitles, text_subtitles


def extract_sub_from_mkv(subtitle_track: dict, mkv_filepath: str, srt_filepath: str):
    """ This functions extract one subtitle track from the mkv to an external file

    :param subtitle_track: Subtitles track as dictionaries
    :param mkv_filepath: Path to the mkv file
    :param srt_filepath: Path to the final srt file expected
    :return: image_sub_filepath - Path to the extracted file
    """

    track_id = subtitle_track['id']

    logging.debug(f"Extracting image sub {track_id} (not PGS) from mkv to file")

    # Filename. The file extension being defined automatically by mkvextract, we only add a "."
    filepath_no_ext = os.path.splitext(srt_filepath)[0] + "."

    output = subprocess.run([Config.MKV_EXTRACT, "tracks", mkv_filepath, f"{track_id}:{filepath_no_ext}"],
                            capture_output=True, encoding="utf-8")

    # ** Finding the new filename extension.
    # We search in the subprocess output the new filename and get its extension
    filename_no_ext = os.path.split(filepath_no_ext)[1]
    extension_begin = output.stdout.find(filename_no_ext) + len(filename_no_ext)  # The ext begins after the filename.
    extension_end = extension_begin + str.find(output.stdout[extension_begin:], " ")  # It ends at the first space

    file_extension = output.stdout[extension_begin:extension_end]
    image_sub_filepath = filepath_no_ext + file_extension

    logging.debug(f"Extracted in {os.path.split(image_sub_filepath)[1]}")

    return image_sub_filepath
