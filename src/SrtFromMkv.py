import glob
import logging
import os
from typing import List

from src.DirectoryHash import has_directory_changed, update_folder_hash
from src.MkvUtils import get_sub_tracks, sort_sutitles, extract_sub_from_mkv
from src.MkvUtils import pgs_codec
from src.SubtitlesConversions import pgs_track_to_srt_file, pgs_file_to_srt, convert_sub_to_pgs


def get_srt_filepath(mkv_filepath: str, subtitle_track: dict):
    """ Identify the name of the final srt file from movie path and subtitle track

    :param mkv_filepath: Path to the mkv file
    :param subtitle_track: Subtitle track as a dictionnary
    :return: srt_filepath - Generated file path for the srt file
    """

    track_num = subtitle_track['properties']["number"]
    lang = subtitle_track['properties']['language']
    # Bug correction for french, being "fre" or "fra" and better recognised as "fr"
    lang = "fr" if lang[:2] == "fr" else lang

    srt_filepath = f"{mkv_filepath[:-4]}.{lang}.track {track_num}.srt"

    return srt_filepath


def convert_mkv_sub_to_srt(mkv_filepath: str, language_to_extract: List[str]) -> [bool, List[str]]:
    """

    :param mkv_filepath: Path to the mkv file
    :param language_to_extract: example : ["eng", "fra"]
    :return: bool_SubtitlesFound, extractedSubtitles - False if the file had no subtitles
    """

    logging.info(f"\nFile - {os.path.split(mkv_filepath)[1]}")

    # Getting the subtitles tracks and sorting it btwn text & image
    subtitles_tracks = get_sub_tracks(mkv_filepath)
    image_subtitles, text_subtitles = sort_sutitles(subtitles_tracks)
    extracted_subtitles = []

    # If both lists are empty, we return False
    if not text_subtitles and not image_subtitles:
        logging.warning(f"*** No Subtitle found in file ***")
        return False, extracted_subtitles

    # We log all the srt files found
    for sub_track in text_subtitles:
        lang = sub_track['properties']['language']
        codec: str = sub_track["properties"]["codec_id"].lower()

        if lang in language_to_extract:
            logging.info(f"\tSubtitle - {lang}, {codec} - Already Text codec !")

    # For each image subtitle
    for sub_track in image_subtitles:

        # We get the path to which the srt file will be saved
        lang = sub_track['properties']['language']
        codec: str = sub_track["properties"]["codec_id"].lower()
        srt_filepath = get_srt_filepath(mkv_filepath, sub_track)

        # If this file already exists, we skip this file and go to the next
        if os.path.exists(srt_filepath):
            logging.info(f"\tSubtitle - {lang}, {codec} - Already Done !")
            continue

        # Otherwise we extract the subtitle if it is one of the wanted languages
        if lang in language_to_extract:
            logging.info(f"\tSubtitle - {lang}, {codec}")

            # If subtitle is PGS, we directly use the correct library
            if codec == pgs_codec:
                pgs_track_to_srt_file(mkv_filepath, sub_track, srt_filepath)

            # Otherwise, we extract the image-sub file, convert it to PGS, and use the library
            else:
                tmp_sub_filepath = extract_sub_from_mkv(sub_track, mkv_filepath, srt_filepath)
                tmp_pgs_filepath = convert_sub_to_pgs(tmp_sub_filepath)
                pgs_file_to_srt(tmp_pgs_filepath, sub_track, srt_filepath)

                # removing temporary files
                os.remove(tmp_sub_filepath)
                os.remove(tmp_pgs_filepath)
                # removing .idx files
                for idx_file in glob.glob(os.path.split(mkv_filepath)[0] + "/*.idx"):
                    os.remove(idx_file)

            # We do some post-processing operation on the text to correct potential errors
            text_subtitles_post_processing(srt_filepath, lang)
            extracted_subtitles.append(srt_filepath)

            logging.info(f"\tDone!")

    return True, extracted_subtitles


def text_subtitles_post_processing(srt_filepath, lang):
    """ Corrects some errors in the text. THose are simple operations

    :param srt_filepath: Path to the srt file
    :param lang: lang of the srt file ("eng", "fr")
    """
    if lang in ["eng", "en"]:
        with open(srt_filepath, mode="r") as file:
            text = file.read()

        new_text = text.replace("|", "I")  # We replace the '|' by 'I' as they are mostly errors

        with open(srt_filepath, mode="w") as file:
            file.write(new_text)


def extract_from_subfolders(root_folder: str):
    """ Extract subtitles from all the mkv files in the subfolders of the given folder
    
    :param root_folder: 
    :return: 
    """
    # noinspection PyArgumentList
    logging.basicConfig(level=logging.INFO, force=True, format='%(message)s')

    logging.info(f"Begining of extraction from {root_folder}")

    if not has_directory_changed(root_folder):
        logging.info(f"Folder has not changed since last extraction. Stopping operatin now.")
        return

    mkv_files_list = glob.glob(os.path.join(root_folder, "**/*.mkv"), recursive=True)

    empty_files = []

    for mkv_filepath in mkv_files_list:
        if os.path.isfile(mkv_filepath):  # Making sure we're not looking at a folder named ***.mkv
            file_extr, _ = convert_mkv_sub_to_srt(mkv_filepath, language_to_extract=["eng", "fra", "fre"])
            if not file_extr:
                empty_files.append(os.path.split(mkv_filepath)[1])

    logging.info(f"List of files with no subtitles : ")
    logging.info("\n\t" + '\n\t'.join(empty_files))

    update_folder_hash(root_folder)
