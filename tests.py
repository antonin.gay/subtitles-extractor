import glob
import logging
import os

from src.SrtFromMkv import convert_mkv_sub_to_srt

# noinspection PyArgumentList
logging.basicConfig(level=logging.DEBUG, filename=r"D:\PythonScript\tests.log", filemode="w", force=True)


def assert_subtitles_files(extracted_subtitles):
    for sub_file in extracted_subtitles:
        assert sub_file[-4:] == ".srt"
        assert os.path.exists(sub_file)


def removing_srt_files(movie_dir):
    srt_filelist = glob.glob(movie_dir + "/*.srt")
    for srt_filepath in srt_filelist:
        os.remove(srt_filepath)

    logging.debug(f"Removed {len(srt_filelist)} srt file(s)")


def test_srt_from_pgs_track():
    # The movie The Hunt only have PGS tracks. It needs to be converted to PGS as a temporary step
    movie_dir = r"D:\Videos\Movies\The Hunt (2012) + Extras (1080p BluRay x265 HEVC 10bit AAC 5.1 afm72)"
    movie_file = r"The Hunt (2012) (1080p BluRay x265 afm72).mkv"
    movie_path = os.path.join(movie_dir, movie_file)

    # Removing previous srt files
    removing_srt_files(movie_dir)

    # Extrating subtitles
    success, extracted_subtitles = convert_mkv_sub_to_srt(movie_path, language_to_extract=["eng", "fra", "fre"])

    assert success
    assert_subtitles_files(extracted_subtitles)


def test_srt_from_not_pgs_track():
    # The movie BlackSwan only have one VobSub track. It needs to be converted to PGS as a temporary step
    movie_dir = r"D:\Videos\Movies\Black Swan (2010) (1080p BluRay x265 HEVC 10bit AAC 5.1 afm72)"
    movie_file = r"Black Swan (2010) (1080p BluRay x265 afm72).mkv"
    movie_path = os.path.join(movie_dir, movie_file)

    # Removing previous srt files
    removing_srt_files(movie_dir)

    # Extrating subtitles
    success, extracted_subtitles = convert_mkv_sub_to_srt(movie_path, language_to_extract=["eng", "fra", "fre"])

    assert success
    assert_subtitles_files(extracted_subtitles)

# todo: implemet tsts for Directory Hash
