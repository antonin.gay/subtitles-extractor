import glob
import os


def replacing_fra_to_fr():
    # Replacing all ".fr*" to ".fr"

    srt_files = glob.glob("D:/**/*.srt", recursive=True)

    for srt_file in srt_files:
        if ".fre.track " in srt_file or ".fra.track " in srt_file:
            new_path = srt_file.replace(".fre.track ", ".fr.track ").replace(".fra.track ", ".fr.track ")

            os.rename(srt_file, new_path)


def move_all_srt_files():
    """ Moves all srt files from D:/Videos/ to D:/Videos_old_srt/. Only does that for srt extracted from mkv"""

    srt_files = glob.glob("D:/**/*.srt", recursive=True)

    for srt_file in srt_files:
        directory = os.path.dirname(srt_file)
        if glob.glob(os.path.join(directory, "*.mkv")):
            # If there is a mkv file, we move the srt file
            new_path = str.replace(srt_file, "Videos", "Videos_old_srt")

            os.makedirs(os.path.dirname(new_path), exist_ok=True)

            os.rename(srt_file, new_path)


if __name__ == '__main__':
    print("Hello World!")

    move_all_srt_files()
