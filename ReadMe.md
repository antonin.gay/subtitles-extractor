# MKV_PGStoSRT

This project extracts image-type subtitles from mkv files and convert them to text-based subtitles ".srt". 

It was created for a Plex library, as the Plex Client app on LG OLED TV would need transcoding
for image-based subtitles.
Hence, converting them to srt files allow direct-stream and reduce the computing effort of 
streaming for the server. This is needed for 4K content.

## Dependencies

* BDSup2Sub - https://github.com/mjuhasz/BDSup2Sub
* PgSToSrt - https://github.com/Tentacule/PgsToSrt
* MkvToolNix (mkvmerge and mkvextract)

The files of the project should be organized like this:

```bash
├── BDSup2Sub\
│   ├── BDSup2Sub.jar
├── PgsToSrt\
│   ├── dll
│   |   ├── PgsToSrt.dll
│   |   ├── ...
├── src
│   ├── ...
├── config.yaml
├── main.py
├── README.md
├── ...
```

However, all the paths can be configured in the config.yaml file

### BDSup2Sub

Put the BDSup2Sub.jar in a BDSup2Sub/ folder in the project directory.

### PgsToSrt

Build the app and put the dll file (with its surrounding generated files) in 
.\PgsToSrt\dll\PgsToSrt.dll

### MkvToolNix

Install it and specify the installation path in the config.yaml file

